import * as pulumi from "@pulumi/pulumi"

import "mocha";
import { expect } from "chai";

pulumi.runtime.setMocks({
  newResource: function (type: string, name: string, inputs: any): { id: string, state: any } {
    return {
      id: inputs.name + "_id",
      state: inputs,
    };
  },
  call: function (token: string, args: any, provider?: string) {
    return args;
  },
});

describe('Heartbeat', function () {
  it('should record incoming requests', async function () {
    expect(true, "First failing test").to.be.false
  })
})