# The Heartbeat Demo

The demo walks through the process of creating a basic AWS serverless application through test-driven development (or design, which might be a slightly more accurate description of the process).
It uses Mocha to create the tests that drive the design, and Pulumi to implement the code that allows the tests to pass.
We'll retstrict ourselves to unit-style tests that depend only on the code and the design. In a real application there would also be a suite of integration tests to prove that our deployed infrastructure does what's expected of it.

## Git tags

This demo repository uses tags to jump from one "interesting" stage of development to the next. This readme will describe what has changed and how it works each time. It might be best to consider this directory "read only"; if you'd like to follow the steps described, you should create a new *dev* directory beside this one (not below it) and work there. At each stage, you can compare your work in the *dev* directory with mine here.

## Creating the project

Following the instructions on the [Pulumi website](https://www.pulumi.com/docs/get-started/install/), we've got our Pulumi Access Token and set it as the environment variable `PULUMI_ACCESS_TOKEN`. Then we login into Pulumi and create a new AWS typescript project:

```
$ pulumi login
Logged into pulumi.com as anonymous (https://app.pulumi.com/anonymous)
$ pulumi new aws-typescript --force
This command will walk you through creating a new Pulumi project.

Enter a value or leave blank to accept the (default), and press <ENTER>.
Press ^C at any time to quit.

project name: (projects) heartbeat
project description: (A minimal AWS TypeScript Pulumi program) AWS serverless app that logs requests
Created project 'heartbeat'

Please enter your desired stack name.
To create a stack in an organization, use the format <org-name>/<stack-name> (e.g. `acmecorp/dev`).
stack name: (dev)
Created stack 'dev'

aws:region: The AWS region to deploy into: (us-east-1) ap-southeast-2
Saved config

Installing dependencies...

audited 105 packages in 1.481s

15 packages are looking for funding
  run `npm fund` for details

found 0 vulnerabilities


┌────────────────────────────────────────────────────────┐
│                npm update check failed                 │
│          Try running with sudo or get access           │
│          to the local update config store via          │
│ sudo chown -R $USER:$(id -gn $USER) /home/node/.config │
└────────────────────────────────────────────────────────┘
Finished installing dependencies

Your new project is ready to go!

To perform an initial deployment, run 'pulumi up'
```

Don't run `pulumi up`, but do feel free to run `pulumi preview`. If you get an error about *AWS AccessKeyID and/or SecretAccessKey*, you haven't got your AWS credentials in your environment. You can set `AWS_PROFILE` to be the name of the AWS profile you want to use. If you don't know what that is, you can follow the [directions on the Pulumi site](https://www.pulumi.com/docs/intro/cloud-providers/aws/setup/) to set your access key and secret directly into your environment.

### Configuring Mocha

This section describes a few tweaks that I've made to the TypeScript environment in order to allow me to use the current version of TypeScript and its modules. It's safe to skip this section, it doesn't add anything to the important Pulumi TDD concepts.

We need a few more libraries for TDD. The [Pulumi site](https://www.pulumi.com/docs/guides/testing/unit/) gives a good introduction. There's an instruction on that page that we'll use to install Mocha, which is popular in with TypeScript and JavaScript users and provides a familiar pattern to users of many test libraries like RSpec and Jasmine. I like fluent assentions like the kinds provided by Chai, so I'll install that too. `ts-node` is also needed so that Mocha can invoke the Pulumi program in the same way that the Pulumi command-line utility would.

```
$ npm install --save-dev mocha @types/mocha ts-node chai @types/chai
```

For our convenience, we'll also create an npm script to run Mocha. That will allow us to use `npm test` or `npm run test` to run the unit tests. This involves editing the `scripts` section of *package.json*, which was created during `pulumi new` and was updated during `npm install`. I'm also going to update both *package.json* (the npm configuration file) and *tsconfig.json* (the typescript configuration file) so that they both use the same directory (`./lib`) and module style (EcmaScript 2017 or `esnext` ) for JavaScript. Finally I'm setting up a second version of *tsconfig.json*, called *tsconfig.test.json* specifically for Mocha, as Mocha doesn't yet support `esnext` modules. All of this is incidental to the TDD and Pulumi work, so I'll leave you look into those files if you're interested in the exact changes.

### Creating our first failing test

With all our dependencies now sorted, we can start driving our design (and develpoment) with tests. `pulumi new` created a starting point for our Pulumi program, but we don't have a starting point for our test code. So I've created a single failed test.

You can now run the test and see it fail.

```
$ npm test
```

Now you can jump onto the next section using `git checkout implementing_the_first_test`.